# Technical limitations

## Rules

Only one player may exist with a given full name at a given time. They may have at most one corresponding Assassin and one corresponding Police.
